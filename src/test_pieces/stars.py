from random import randrange, choice


class Star(object):

    """
    Represents an individual star within the field
    """

    _PIXELS_PER_SECOND = 30
    _DIMMEST_COLOUR = 50
    _MAX_COLOUR_OFFSET = 66

    def __init__(self, max_x: int, max_y: int, max_depth: int):

        self.x = 0
        self.y = -1
        self.layer = 0
        self.colour = (0, 0, 0)

        self.initialise(max_x, max_y, max_depth)

    def move(self, elapsed_time: int) -> None:

        """
        Moves a star based on a pixels per second delta.
            Smooths movement compared to a static speed
        :param elapsed_time:
        :return: None
        """

        self.y += self.layer + (Star._PIXELS_PER_SECOND * elapsed_time / 1000.0)

    def out_of_bounds(self, max_y: int) -> bool:

        """
        Checks whether the star's Y position is within the specified bounds
        :param max_y:
        :return: bool
        """
        return self.y >= max_y or self.y < 0

    def initialise(self, max_x: int, max_y: int, max_depth: int) -> None:

        """
        Initialise a star
        :param max_x: Width of star field
        :param max_y: Height of star field
        :param max_depth: Depth of star field
        :return: None
        """

        self.x = randrange(0, max_x - 1)
        self.y = randrange(0, max_y - 1) if max_y > 0 else 0
        self.layer = choice(range(1, max_depth + 1))

        red = Star._DIMMEST_COLOUR * self.layer + randrange(0, Star._MAX_COLOUR_OFFSET)
        if red > 255:
            red = 255

        blue = Star._DIMMEST_COLOUR * self.layer + randrange(0, Star._MAX_COLOUR_OFFSET)
        if blue > 255:
            blue = 255

        green = Star._DIMMEST_COLOUR * self.layer + randrange(0, Star._MAX_COLOUR_OFFSET)
        if green > 255:
            green = 255

        self.colour = (red, green, blue)


class StarField(object):

    """
    Represents a field of stars
    """

    _MAX_STARS = 500
    _MAX_DEPTH = 5

    def __init__(self, max_x: int, max_y: int, max_depth: int):

        if max_depth > StarField._MAX_DEPTH:
            max_depth = StarField._MAX_DEPTH

        self._stars = [Star(max_x, max_y, max_depth) for _ in range(StarField._MAX_STARS)]

    def __iter__(self):

        """
        Allows the star field to be iterated
            (for star in stars)
        :return: Iterator
        """

        return iter(self._stars)


if __name__ == "__main__":

    import pygame
    from pygame.locals import *

    pygame.init()
    screen = pygame.display.set_mode((800, 600))
    pygame.display.set_caption("Parallax star field test")
    clock = pygame.time.Clock()

    elapsed = 1

    stars = StarField(800, 600, 3)

    while True:

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                quit()

        screen.lock()
        screen.fill((0, 0, 0))
        for star in stars:
            star.move(elapsed)
            if star.out_of_bounds(768):
                star.initialise(1024, 0, 3)

            screen.fill(star.colour, (star.x, star.y, star.layer, star.layer))

        pygame.display.update()
        screen.unlock()

        elapsed = clock.tick(30)

